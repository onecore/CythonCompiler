__author__ = ['Mark Anthony Pequeras']
__version__ = [1.0]
__license__ = ['MIT']
__website__ = 'http://www.marp.me/'


# {
import sys
global fr
fr = ""
from time import sleep
from subprocess import call

help = """
ERROR:
    python build.py FILENAME
"""
try:
    fs = sys.argv[1]
    cache = open(fs+".pyx","w")
    orgCache = open(fs+".py","r")
    cache.write(orgCache.read())
    cache.close()
    orgCache.close()

    f = ".pyfile"
    fo = open(f,"w")
    fw = fo.write(fs+str(".pyx"))
    fo.close()
    print "\n\nCompiling '{}.py'........\n\n".format(fs)
    fr = str(fs)
    sleep(3)
    call(["python2.7","CCompiler.py","build_ext","--inplace"])

except:
    print help




call(["rm",fr+".c"]) # just Comment to disable C Code Generation deletion
call(["rm",fr+".pyx"])  # just Comment to disable .pyx file deletion

print "\n\nBuild Successful!\n"

call(["ls","-l"])


