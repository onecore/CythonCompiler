__author__ = ['Mark Anthony Pequeras']
__version__ = [1.0]
__license__ = ['MIT']
__website__ = 'http://www.marp.me/'

import subprocess
f = open(".pyfile","r")
PythonFile = f.read() 
f.close()

from distutils.core import setup
from Cython.Build import cythonize


setup(
    ext_modules = cythonize(PythonFile)
    )

