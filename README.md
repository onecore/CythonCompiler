**USAGE:**
    
    - python compile.py NAMEOFFILE (without .py extention)
    
**REQUIREMENTS**

    - Cython Version > 17
    - Mac OSX or Linux OS.
    
**WHATITDOES**

    - This scripts will just simple compile your *.py file into .so
    - removes .C and .Pyx files Generation
    - created it for my own needs :)